# s153277
Feature: TokenService feature

  Scenario: TokenRequested Assignment
    When a "TokensRequested" event for the token service is received
    Then five tokens are created using the customer id "customer"
    And five random unique ids
    And they are stored in the token repository
    And the "TokensProvided" event is sent with the five tokens provided
  