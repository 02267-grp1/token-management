package behaviortests;

import dk.dtu.TokenService;
import dk.dtu.models.Token;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.Event;
import messaging.MessageQueue;
import org.junit.Assert;

import java.util.*;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class TokenServiceSteps {
    MessageQueue queue = mock(MessageQueue.class);
    TokenService ts = new TokenService(queue);
    String customerId;
    ArrayList<Token> expected;
    @When("a {string} event for the token service is received")
    public void aEventForTheTokenServiceIsReceived(String eventName) {
        customerId = "customer";
        ts.handleTokenRequested(new Event(eventName, new Object[]{customerId}));
    }
    @Then("five tokens are created using the customer id {string}")
    public void fiveTokensAreCreatedUsingTheCustomerId(String customerId) {
        expected = ts.getTs().getAll();
        for(Token t : expected) {
            Assert.assertEquals(customerId, t.getCustomerId());
        }
        Assert.assertEquals(5, expected.size());
    }

    @And("five random unique ids")
    public void fiveRandomUniqueIds() {
        List<UUID> uuidList = expected.stream().map(Token::getUuid).toList();
        Set<UUID> uniqueElements = new HashSet<>();
        boolean hasDuplicates = uuidList.stream()
                .anyMatch(e -> !uniqueElements.add(e));
        Assert.assertFalse(hasDuplicates);
    }

    @And("they are stored in the token repository")
    public void theyAreStoredInTheTokenRepository() {
        Assert.assertArrayEquals(expected.toArray(), ts.getTs().getAll().toArray(new Token[0]));
    }


    @And("the {string} event is sent with the five tokens provided")
    public void theEventIsSentWithTheFiveTokensProvided(String eventName) {
        var event = new Event(eventName, new Object[] {expected});
        verify(queue).publish(event);
    }
}
