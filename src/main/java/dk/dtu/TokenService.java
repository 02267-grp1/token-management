package dk.dtu;

import dk.dtu.models.Token;
import lombok.Getter;
import messaging.Event;
import messaging.MessageQueue;

import java.util.ArrayList;
import java.util.UUID;

public class TokenService {
    MessageQueue queue;

    @Getter
    TokenRepository ts;
    /**
        @author s153272
    */
    public TokenService(MessageQueue q) {
        this.queue = q;
        this.queue.addHandler("TokensRequested", this::handleTokenRequested);
        this.queue.addHandler("TokenValidationRequested", this::handleTokenValidation);
        this.queue.addHandler("TokenConsumeRequested", this::handleTokenConsume);
        this.queue.addHandler("TokenToCustomerIdRequested", this::handleTokenToCustomerId);
        this.ts = new TokenStore();
        System.out.println("Token Service Initialized");
    }
    /**
        @author s153272
    */
    public void handleTokenRequested(Event ev) {
        var s = ev.getArgument(0, String.class);
        ArrayList<Token> newTokens = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            Token newToken = new Token(s, UUID.randomUUID());
            newTokens.add(newToken);
            ts.insert(newToken);
        }
        Event event = new Event("TokensProvided", new Object[] { newTokens });
        queue.publish(event);
    }
    /**
        @author s153272
    */
    public void handleTokenValidation(Event ev) {
        var s = ev.getArgument(0, Token.class);
        boolean result = ts.validate(s);
        Event event = new Event("TokenValidationResult", new Object[]{ result });
        queue.publish(event);
    }
    /**
        @author s223142
    */
    public void handleTokenConsume(Event ev) {
        var s = ev.getArgument(0, Token.class);
        boolean result = ts.delete(s);
        Event event = new Event("TokenDeletionResult", new Object[]{ result });
        queue.publish(event);
    }
    /**
        @author s223142
    */
    public void handleTokenToCustomerId(Event ev) {
        var uuid = ev.getArgument(0, UUID.class);
        String result = ts.get(uuid);
        Event event = new Event("TokenToCustomerIdResult", new Object[]{ result });
        queue.publish(event);
    }

}
