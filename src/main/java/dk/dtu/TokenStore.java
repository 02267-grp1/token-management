package dk.dtu;

import dk.dtu.models.Token;

import java.util.ArrayList;
import java.util.UUID;
/**
    The {@link TokenRepository} interface documents the author of each method
 */
public class TokenStore implements TokenRepository{
    private ArrayList<Token> tokens;

    public TokenStore() {
        this.tokens = new ArrayList<>();
    }


    @Override
    public void insert(Token token) {
        this.tokens.add(token);
    }

    @Override
    public boolean validate(Token token) {
        return this.tokens.contains(token);
    }

    @Override
    public boolean delete(Token token) {
        return this.tokens.remove(token);
    }

    @Override
    public String get(UUID uuid) {
        for (Token token : this.tokens) {
            if (token.getUuid().equals(uuid)) {
                return token.getCustomerId();
            }
        }
        return null;
    }

    @Override
    public ArrayList<Token> getAll() {
        return tokens;
    }
}
