package dk.dtu;

import messaging.implementations.RabbitMqQueue;
/**
    @author s153272
*/
public class TokenStartup {
    public static void main(String[] args) throws Exception{
        new TokenStartup().startUp();
    }

    private void startUp() throws Exception {
        System.out.println("startup");
        var mq = new RabbitMqQueue("rabbitmq");
        new TokenService(mq);
    }
}
