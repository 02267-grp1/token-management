package dk.dtu;

import dk.dtu.models.Token;

import java.util.ArrayList;
import java.util.UUID;


public interface TokenRepository {
    /**
        @author s153272
    */
    public void insert(Token token);
    /**
        @author s202082
    */
    public boolean validate(Token token);
    /**
        @author s223142
    */
    public boolean delete(Token token);
    /**
        @author s222994
    */
    public String get(UUID uuid);
    // For testing purposes in order to ensure that the tokens sent with events are also stored in the TokenStore
    public ArrayList<Token> getAll();
}
